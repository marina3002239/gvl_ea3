	const canvas = document.getElementById('glCanvas');
	const gl = canvas.getContext('webgl', { antialias: false });
	
	const mat4 = glMatrix.mat4;

	if (!gl) {
		alert('WebGL is not supported by your browser.');
	}

    const vsSource = `
    attribute vec4 aVertexPosition;
    attribute vec4 aVertexColor;

    uniform mat4 uModelViewMatrix;
    uniform mat4 uProjectionMatrix;

    varying lowp vec4 vColor;

    void main(void) {
      gl_Position = uProjectionMatrix * uModelViewMatrix * aVertexPosition;
      vColor = aVertexColor;
	  gl_PointSize = 3.0;
    }
	`;

	  
	const fsSource = `
    varying lowp vec4 vColor;

    void main(void) {
      gl_FragColor = vColor;
    }
	`;

	const vertexShader = gl.createShader(gl.VERTEX_SHADER);
	gl.shaderSource(vertexShader, vsSource);
	gl.compileShader(vertexShader);

	const fragmentShader = gl.createShader(gl.FRAGMENT_SHADER);
	gl.shaderSource(fragmentShader, fsSource);
	gl.compileShader(fragmentShader);

	const shaderProgram = gl.createProgram();
	gl.attachShader(shaderProgram, vertexShader);
	gl.attachShader(shaderProgram, fragmentShader);
	gl.linkProgram(shaderProgram);

	// Capturing pointers to the Vertex Mesh, Projection Matrix and Model View Matrix
    const programInfo = {
        program: shaderProgram,
        attribLocations: {
            vertexPosition: gl.getAttribLocation(shaderProgram, 'aVertexPosition'),
			vertexColor: gl.getAttribLocation(shaderProgram, "aVertexColor"),
        },
        uniformLocations: {
            projectionMatrix: gl.getUniformLocation(shaderProgram, 'uProjectionMatrix'),
            modelViewMatrix: gl.getUniformLocation(shaderProgram, 'uModelViewMatrix'),
        },
    };

	const positionAttributeLocation = gl.getAttribLocation(shaderProgram, 'aVertexPosition');
	const colorAttributeLocation = gl.getAttribLocation(shaderProgram, 'aVertexColor');

	const positionBuffer = gl.createBuffer();
	const colorBuffer = gl.createBuffer();
	const indexBuffer = gl.createBuffer();

	gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);

	const positions = [];
	const colors = [];
	const indices = [];
	const vertexIndices = [];
	let sizeFactor = 5;

	vertexList = [
	[0, 0.05, 0]
	,[0.4, 0.6, 0]
	,[1, 0.6, 0]
	,[1, 0.2, 0]
	,[0.85, 0.2, 0]
	,[0.85, -0.45, 0]
	,[1, -0.45, 0]
	,[1, -0.85, 0]
	,[0.25, -0.85, 0]
	,[0.25, -0.45, 0]
	,[0.4, -0.45, 0]
	,[0.4, -0.05, 0]
	,[0, -0.6, 0]
	,[-0.4, 0.6, 0]
	,[-1, 0.6, 0]
	,[-1, 0.2, 0]
	,[-0.85, 0.2, 0]
	,[-0.85, -0.45, 0]
	,[-1, -0.45, 0]
	,[-1, -0.85, 0]
	,[-0.25, -0.85, 0]
	,[-0.25, -0.45, 0]
	,[-0.4, -0.45, 0]
	,[-0.4, -0.05, 0]

	,[-1, -0.9, 0]
	,[-0.1, -0.9, 0]
	,[-1, -1.0, 0]
	,[-0.1, -1.0, 0]
	
	,[0.1, -0.9, 0]
	,[1.0, -0.9, 0]
	,[1.0, -1.0, 0]
	,[0.1, -1.0, 0]

	];
		
	colorList = [
	[1, 0.696, 0.0196, 1]
	,[1, 0.496, 0.0196, 1]
	,[1, 0.996, 0.0196, 1]
	,[1, 0.896, 0.0196, 1]
	,[1, 0.396, 0.0196, 1]
	,[1, 0.496, 0.0196, 1]
	,[1, 0.796, 0.0196, 1]
	,[0.796, 0.796, 0.0196, 1]
	,[0.896, 0.896, 0.0196, 1]
	,[0.896, 0.996, 0.0196, 1]
	,[0.896, 0.396, 0.0196, 1]
	,[0.0196, 0.296, 0.0196, 1]
	,[0.0196, 0.596, 0.0196, 1]
	,[1, 0.896, 0.0196, 1]
	,[1, 0.596, 0.0196, 1]
	,[1, 0.396, 0.0196, 1]
	,[1, 0.796, 0.0196, 1]
	,[1, 0.996, 0.0196, 1]
	,[1, 1, 0.0196, 1]
	,[0.0196, 0.396, 0.0196, 1]
	,[1, 0.296, 0.0196, 1]
	,[1, 0.896, 0.0196, 1]
	,[1, 0.496, 0.0196, 1]
	,[1, 0.996, 0.0196, 1]
	
	,[1, 0.296, 0.0196, 1]
	,[1, 0.896, 0.0196, 1]
	,[1, 0.496, 0.0196, 1]
	,[1, 0.996, 0.0196, 1]

	,[1, 0.296, 0.0196, 1]
	,[1, 0.896, 0.0196, 1]
	,[1, 0.496, 0.0196, 1]
	,[1, 0.996, 0.0196, 1]
	];
	
    triangles =[
	0,1,11
	,0,11,12
	,1,2,4
	,2,3,4
	,4,5,10
	,1,4,10
	,6,7,9
	,7,8,9
	,0,13,23
	,0,23,12
	,13,14,16
	,14,15,16
	,16,17,22
	,13,16,22
	,18,19,21
	,19,20,21
	
	,24,25,26
	,25,27,26

	,28,29,30
	,28,30,31
	];

	for (let i = 0; i < vertexList.length; i++) {
		positions.push(vertexList[i][0] * sizeFactor, vertexList[i][1] * sizeFactor, vertexList[i][2] * sizeFactor);
		colors.push(colorList[i][0], colorList[i][1], colorList[i][2], 1.0);
		vertexIndices.push(i);
	}

	for (let i = 0; i < triangles.length; i += 3) {
		// Define the triangles by adding indices
		indices.push(triangles[i], triangles[i + 1], triangles[i + 2]);
	}

	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);
	gl.vertexAttribPointer(positionAttributeLocation, 3, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(positionAttributeLocation);

	gl.bindBuffer(gl.ARRAY_BUFFER, colorBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(colors), gl.STATIC_DRAW);
	gl.vertexAttribPointer(colorAttributeLocation, 4, gl.FLOAT, false, 0, 0);
	gl.enableVertexAttribArray(colorAttributeLocation);

	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(indices), gl.STATIC_DRAW);
	
	// Clear the world previous graphics and depth buffer
    gl.clearColor(1.0, 1.0, 1.0, 1.0);
    gl.clearDepth(1.0);
    gl.enable(gl.DEPTH_TEST);
    gl.depthFunc(gl.LEQUAL);
    gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);

	// Prepare the camera viewport and projection matrix
    const fieldOfView = 90 * Math.PI / 180;   // in radians
    const aspect = gl.canvas.clientWidth / gl.canvas.clientHeight;
    const zNear = 0.1;
    const zFar = 100.0;
    const projectionMatrix = mat4.create();
    mat4.perspective(projectionMatrix,
        fieldOfView,
        aspect,
        zNear,
        zFar);

	// Prepare the world model view matrix
    const modelViewMatrix = mat4.create();
    mat4.translate(modelViewMatrix, modelViewMatrix, [-0.0, 0.0, -6.0]);

	gl.useProgram(programInfo.program);

	// Transform projection matrix of the program
    gl.uniformMatrix4fv(
        programInfo.uniformLocations.projectionMatrix,
        false,
        projectionMatrix);
	// Transform model matrix of the program
    gl.uniformMatrix4fv(
        programInfo.uniformLocations.modelViewMatrix,
        false,
        modelViewMatrix);
	
	// Draw the sphere using indices
	gl.drawElements(gl.TRIANGLES, indices.length, gl.UNSIGNED_SHORT, 0);

	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, null);
	gl.bindBuffer(gl.ARRAY_BUFFER, null);
	gl.bindBuffer(gl.ARRAY_BUFFER, positionBuffer);
	gl.bufferData(gl.ARRAY_BUFFER, new Float32Array(positions), gl.STATIC_DRAW);
	gl.bindBuffer(gl.ELEMENT_ARRAY_BUFFER, indexBuffer);
	gl.bufferData(gl.ELEMENT_ARRAY_BUFFER, new Uint16Array(vertexIndices), gl.STATIC_DRAW);
	gl.disableVertexAttribArray(colorAttributeLocation);
	gl.vertexAttrib4f(colorAttributeLocation, 1.0, 0, 0, 1.0);	
	gl.drawArrays(gl.POINTS, 0, vertexIndices.length);
